<?php namespace Tribaltactics\Oauthclient;

use Config,
	Session,
	Tribaltactics\Oauthclient\Interfaces\OAuthUserServiceInterface;;	

class OAuthUserService implements OAuthUserServiceInterface {
	
	public function findUserByUserIdentifier($id){		
		if ($userData = Session::get('userData')){
			return $user = new OAuthUser($userData);
		}
		OAuthClient::setToken(Session::get('oauth_token'), Session::get('oauth_token_secret'));
		OAuthClient::fetch(OAuthClient::getBaseUrl() . "/secure/users/".$id, null, OAUTH_HTTP_METHOD_GET);		
		$response = json_decode(OAuthClient::getLastResponse(), true);

		$userData = array(
			"id" => $response["results"][0]["id"],
			"name" => $response["results"][0]["username"]
		);
		Session::put("userData", $userData);
		$user = new OAuthUser($userData);
		
		Session::put("user", $response["results"][0]);
		
		return $user;
	}

	/**
	* Find the user by its username
	*
	* @param string username
	*
	* @return object user
	*/
	public function findUserByUsername($username){
		$this->client->fetch($this->client->getBaseUrl() . "/users/username/".$id, null, OAUTH_HTTP_METHOD_GET);		

		$response = json_decode($this->client->getLastResponse(), true);
		
		$user = new OAuthUser($response["data"]);

		return $user;
	}


}