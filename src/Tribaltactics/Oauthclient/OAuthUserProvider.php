<?php namespace Tribaltactics\Oauthclient;

use Session,
    Illuminate\Auth\UserProviderInterface;

class OAuthUserProvider
    implements UserProviderInterface
{
    /**
    * @var UserService
    */
    private $userService;

    public function __construct(OAuthUserService $userService)
    {
        $this->userService = $userService;
    } 

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     *
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveById($identifier)
    {
        
        $user = $this->userService->findUserByUserIdentifier($identifier);

        if (!$user instanceof OAuthUser) {
            return false;
        }

        return $user;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     *
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        /** @var User $user  */
        $user = $this->userService->findUserByUserName($credentials['username']);

        if (!$user instanceof OAuthUser) {
            return false;
        }

        return $user;    
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Auth\UserInterface $user
     * @param  array  $credentials
     *
     * @return bool
     */
     public function validateCredentials(\Illuminate\Auth\UserInterface $user, array $credentials)
     {
         $validated = $this->userService->validateUserCredentials(
             $credentials['username'],
             $credentials['password']
         );

         $validated = $validated && $user->userName = $credentials['username'];

         return $validated;
     }
}