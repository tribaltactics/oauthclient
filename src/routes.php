<?php

Route::get('/login', function(){
	try{
		OAuthClient::enableDebug();
		$info = OAuthClient::getRequestToken(OAuthClient::getRequestTokenEndpoint() . "?oauth_callback=" . Config::get("oauth.callbackUrl"));
		
		Session::put("authentification_url", $info["authentification_url"]);
		Session::put("oauth_token", $info["oauth_token"]);
		Session::put("oauth_token_secret", $info["oauth_token_secret"]);

		return Redirect::to(Session::get('authentification_url') . "?oauth_token=" . Session::get('oauth_token'));				
	} catch (OAuthException $e){
		echo $e->getMessage();
		echo $e->lastResponse;
	}
});

Route::get("/callback", function(){	
	try{
		// Get the UserId from the header to avoid making another request for the current user.
		OAuthClient::enableDebug();
		OAuthClient::setToken($_REQUEST['oauth_token'], Session::get('oauth_token_secret') );
		$info = OAuthClient::getAccessToken(OAuthClient::getAccessTokenEndpoint(), null, $_REQUEST['oauth_verifier']);

		Session::put("oauth_token", $info["oauth_token"]);
		Session::put("oauth_token_secret", $info["oauth_token_secret"]);

		Auth::loginUsingId(Input::get("user_id"));
		
		return Redirect::to("/");

	} catch (OAuthException $e){
		echo $e->getMessage();
		echo $e->lastResponse;

	}
});