<?php namespace Tribaltactics\Oauthclient;

use Illuminate\Auth\UserInterface,
	Illuminate\Auth\GenericUser;

class OAuthUser extends GenericUser implements UserInterface {

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->attributes['id'];
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->attributes['password'];
	}

}