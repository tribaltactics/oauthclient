<?php namespace Tribaltactics\Oauthclient;

use Illuminate\Support\ServiceProvider,
	OAuth, Config;

class OAuthClientServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot(){
		$this->package('tribaltactics/oauthclient');

		include __DIR__.'/../../routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['OAuthClient'] = $this->app->share(function($app)
		{
			return new OAuthClient();
		});		
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('OAuthClient');
	}

}