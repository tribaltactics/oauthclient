<?php namespace Tribaltactics\Oauthclient\Interfaces;

interface OAuthUserServiceInterface {
	/**
    * Find the user by its identifier
    * 
    * @param string id
    *
    * @return object user
    */
    public function findUserByUserIdentifier($id);

    /**
    * Find the user by its username
    *
    * @param string username
    *
    * @return object user
    */
    public function findUserByUsername($username);

}